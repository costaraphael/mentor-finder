defmodule MentorFinder.Repo do
  use Ecto.Repo,
    otp_app: :mentor_finder,
    adapter: Ecto.Adapters.Postgres
end
