defmodule MentorFinderWeb.Router do
  use MentorFinderWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {MentorFinderWeb.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug :fetch_current_user
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", MentorFinderWeb do
    pipe_through [:browser, :require_authenticated_user]

    live "/", PageLive, :index

    live_dashboard "/_dashboard", metrics: MentorFinderWeb.Telemetry
  end

  # Other scopes may use custom stacks.
  # scope "/api", MentorFinderWeb do
  #   pipe_through :api
  # end

  ## Authentication routes

  scope "/", MentorFinderWeb do
    pipe_through [:browser, :redirect_if_user_is_authenticated]

    get "/users/register", UserRegistrationController, :new
    post "/users/register", UserRegistrationController, :create
    get "/users/log-in", UserSessionController, :new
    post "/users/log-in", UserSessionController, :create
    get "/users/reset-password", UserResetPasswordController, :new
    post "/users/reset-password", UserResetPasswordController, :create
    get "/users/reset-password/:token", UserResetPasswordController, :edit
    put "/users/reset-password/:token", UserResetPasswordController, :update
  end

  scope "/", MentorFinderWeb do
    pipe_through [:browser, :require_authenticated_user]

    get "/users/settings", UserSettingsController, :edit
    put "/users/settings/update-password", UserSettingsController, :update_password
    put "/users/settings/update-email", UserSettingsController, :update_email
    get "/users/settings/confirm-email/:token", UserSettingsController, :confirm_email
  end

  scope "/", MentorFinderWeb do
    pipe_through [:browser]

    delete "/users/log_out", UserSessionController, :delete
    get "/users/confirm", UserConfirmationController, :new
    post "/users/confirm", UserConfirmationController, :create
    get "/users/confirm/:token", UserConfirmationController, :confirm
  end
end
