# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :mentor_finder,
  ecto_repos: [MentorFinder.Repo],
  generators: [binary_id: true]

# Configures the endpoint
config :mentor_finder, MentorFinderWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "xaekCrHxBIpl3t2ZBUkELCEVt2NdI1sBTfrRDs5ppFT7gGDNsCcHRFpB3jLKwGiP",
  render_errors: [view: MentorFinderWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: MentorFinder.PubSub,
  live_view: [signing_salt: "GKBZ7A7v"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
